# `serve`

A lightweight utility to serve the current directory.

This is useful for quickly serving the current directory over HTTP, where you want an HTTP server, for instance to replace calls to `python -mhttp.server` or `npx http-server`.

## Installation

```sh
go install gitlab.com/tanna.dev/serve@HEAD
```

## Running

```sh
cd /path/to/public
serve
```

Then, go to `http://localhost:8080` and you'll be able to view the current directory's listing, and then can choose a file to view.
